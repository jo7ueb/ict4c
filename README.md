# [ICT4C: ねこでも知りたいICT入門 ～DX実現のために知っておきたい情報通信技術の話～](https://ict4c.yendo.info)

[![Netlify Status](https://api.netlify.com/api/v1/badges/e356de96-9d5f-4ca4-ba12-7ca35f0bfd64/deploy-status)](https://app.netlify.com/sites/ict4c/deploys)

[ICT4C: ねこでも知りたいICT入門 ～DX実現のために知っておきたい情報通信技術の話～](https://ict4c.yendo.info) のソースコードです。
このサイトは MkDocs を用いて作成され、Netlify でホスティングされています。

誤記の指摘や内容のリクエストは、Issue or MR にてお願いいたします。
